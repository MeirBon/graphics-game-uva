/*
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 02-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 31-10-2017
 * Comments ........
 *
 */

function myOrtho(left, right, bottom, top, near, far) {
    /*
     * Set the size of the model for our view.
     */
    var modelview = [
        2/(right-left), 0.0, 0.0, 0.0,
        0.0, 2/(top-bottom), 0.0, 0.0,
        0.0, 0.0, 2/(near-far), 0.0,
        0.0, 0.0, 0.0, 1.0
    ];

    /*
     * And set the projection position.
     */
    var projection = [
        1.0 ,0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        -(left+right)/2.0, -(top+bottom)/2.0, -(far+near)/2.0, 1.0
    ];

    /*
     * Multiply the two views and return.
     */
    return m4.multiply(projection, modelview);
}
