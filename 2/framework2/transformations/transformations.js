/*
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 02-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 31-10-2017
 * Comments ........
 *
 */

function myTranslate(x, y, z) {
    // Translate by x, y and z.
    // Matrix looks transposed, but that is just syntax.
    var mat = [
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        x, y, z, 1.0
    ];

    return mat;
}

function myScale(x, y, z) {
    // Scale by x, y and z.
    var mat = [
        x, 0.0, 0.0, 0.0,
        0.0, y, 0.0, 0.0,
        0.0, 0.0, z, 0.0,
        0.0, 0.0, 0.0, 1.0
    ];

    return mat;
}

function myRotate(angle, x, y, z) {
    // Rotate by angle around [x, y, z]^T.
    var lengthOf = function (vector) {
        return Math.sqrt(Math.pow(vector[0], 2) + Math.pow(vector[1], 2) + Math.pow(vector[2], 2));
    };

    //
    // 1. Create the orthonormal basis
    //

    var w = [x, y, z];
    var length_w = lengthOf(w);

    w.forEach(function (value, index) {
        w[index] /= length_w;
    });

    var tmp = [w[0], w[1], w[2]];

    var smallest = tmp[0];
    var indexOf = 0;
    tmp.forEach(function (value, index) {
        if (value < smallest) {
            smallest = value;
            indexOf = index;
        }
    });
    tmp[indexOf] = 1;

    /**
     * ( ay*bz - az*by )
     * ( az*bx - ax*bz )
     * ( ax*by - ay*bx )
     */
    var u = [
        w[1]*tmp[2] - w[2]*tmp[1],
        w[2]*tmp[0] - w[0]*tmp[2],
        w[0]*tmp[1] - w[1]*tmp[0]
    ];

    var length_u = lengthOf(u);
    u.forEach(function (value, index) {
        u[index] /= length_u;
    });

    // v = w x u
    var v = [
        w[1]*u[2] - w[2]*u[1],
        w[2]*u[0] - w[0]*u[2],
        w[0]*u[1] - w[1]*u[0]
    ];

    //
    // 2. Set up the three matrices making up the rotation
    //

    var A = [
        u[0], u[1], u[2], 0.0,
        v[0], v[1], v[2], 0.0,
        w[0], w[1], w[2], 0.0,
        0.0, 0.0, 0.0, 1.0
    ];

    var B = [
        Math.cos(angle), Math.sin(angle), 0.0, 0.0,
        -Math.sin(angle), Math.cos(angle), 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    ];

    var C = [
        u[0], v[0], w[0], 0.0,
        u[1], v[1], w[1], 0.0,
        u[2], v[2], w[2], 0.0,
        0.0, 0.0, 0.0, 1.0
    ];


    var mat = m4.multiply(A, m4.multiply(B, C));
    return mat;
}
