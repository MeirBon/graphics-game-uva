/*
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 02-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 31-10-2017
 * Comments ........
 *
 */

function myLookAt(eyeX, eyeY, eyeZ,
                  centerX, centerY, centerZ,
                  upX, upY, upZ) {

    /**
     * Calculates length of a vector
     */
    var lengthOf = function (vector) {
        return Math.sqrt(Math.pow(vector[0], 2) + Math.pow(vector[1], 2) + Math.pow(vector[2], 2));
    };

    /**
     * Normalizes vector
     */
    var normalize = function(vector) {
        var length = lengthOf(vector);
        vector.forEach(function (value, index) {
            vector[index] = value / length;
        });
        return vector;
    };

    /**
     * Calculates cross-product of 2 vectors
     */
    var crossProduct = function (a, b) {
       return [
            // ( ay*bz - az*by )
            a[1]*b[2] - a[2]*b[1],
            // ( az*bx - ax*bz )
            a[2]*b[0] - a[0]*b[2],
            // ( ax*by - ay*bx )
            a[0]*b[1] - a[1]*b[0]
       ];
    };

    var n = normalize([
        eyeX - centerX,
        eyeY - centerY,
        eyeZ - centerZ
    ]);

    var u = normalize(crossProduct([upX, upY, upZ], n));

    var v = normalize(crossProduct(n, u));

    // Final matrix of u, v, n
    var mat = [
        u[0], v[0], n[0], 0.0,
        u[1], v[1], n[1], 0.0,
        u[2], v[2], n[2], 0.0,
        0.0, 0.0, 0.0, 1.0
    ];

    // Need to translate scene to camera position
    var translation = [
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        -eyeX, -eyeY, -eyeZ, 1.0
    ];

    // Multiply matrices
    return m4.multiply(mat, translation);
}
