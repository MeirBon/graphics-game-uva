/* Computer Graphics and Game Technology, Assignment Ray-tracing
 *
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 23-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 23-11-2017
 * Comments ........
 *
 *
 * (always fill in these fields before submitting!!)
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "shaders.h"
#include "perlin.h"
#include "v3math.h"
#include "intersection.h"
#include "scene.h"
#include "quat.h"
#include "constants.h"

// shade_constant()
//
// Always return the same color. This shader does no real computations
// based on normal, light position, etc. As such, it merely creates
// a "silhouette" of an object.

vec3
shade_constant(intersection_point ip)
{
    return v3_create(1, 0, 0);
}

vec3
shade_matte(intersection_point ip)
{
    // Start with ambient light color
    vec3 color = v3_create(
        scene_ambient_light,
        scene_ambient_light,
        scene_ambient_light
    );

    // Loop over all light sources
    for (int i = 0; i < scene_num_lights; i++) {
        // Calculate the color of light
        light light_source = scene_lights[i];
        vec3 l = v3_normalize(v3_subtract(light_source.position, ip.p));

        float c = light_source.intensity * fmax(0, v3_dotprod(l, ip.n));

        // Add a small offset to prevent self-shadowing
        vec3 ray_origin = v3_add(ip.p, v3_multiply(l, 0.0001));

        // Check shadows to prevent self-shadowing
        if (shadow_check(ray_origin, l)) continue;

        c = light_source.intensity * fmax(0, v3_dotprod(ip.n, l));
        color.x += c;
        color.y += c;
        color.z += c;
    }

    // Normalize color
    float max = fmax(color.x, fmax(color.y, color.z));
    if (max > 1.0) {
        color.x = color.x / max;
        color.y = color.y / max;
        color.z = color.z / max;
    }

    return color;
}

vec3
shade_blinn_phong(intersection_point ip)
{
    float k_diffuse = 0.8;
    float k_specular = 0.5;
    int alpha = 50;

    vec3 cd = v3_create(1, 0, 0);
    vec3 cs = v3_create(1, 1, 1);

    float diffuse_contribution = 0.0;
    float specular_contribution = 0.0;

    for (int i = 0; i < scene_num_lights; i++) {
        light light_source = scene_lights[i];
        vec3 l = v3_normalize(v3_subtract(light_source.position, ip.p));
        // Calculate half way vector
        vec3 h = v3_normalize(v3_add(l, ip.i));
        vec3 ray_origin = v3_add(ip.p, v3_multiply(ip.n, 0.0001));

        if (shadow_check(ray_origin, l)) continue;

        // cl *  max(0, n.l)
        diffuse_contribution += light_source.intensity * fmax(0, v3_dotprod(ip.n, l));
        // cl * (h.n)**p
        specular_contribution += light_source.intensity * pow(v3_dotprod(ip.n, h), alpha);
    }

    diffuse_contribution *= k_diffuse;
    diffuse_contribution += scene_ambient_light;
    specular_contribution *= k_specular;

    vec3 color = v3_add(
        v3_multiply(cd, diffuse_contribution),
        v3_multiply(cs, specular_contribution)
    );

    // Normalize color
    float max = fmax(color.x, fmax(color.y, color.z));
    if (max > 1.0) {
        color.x = color.x / max;
        color.y = color.y / max;
        color.z = color.z / max;
    }

    return color;
}

vec3
shade_reflection(intersection_point ip)
{
    // Calculate the reflection direction with r = 2 * n * (i * n) - i
    vec3 r = v3_subtract(v3_multiply(v3_multiply(ip.n, 2),
                                     v3_dotprod(ip.i, ip.n)), ip.i);
    // Get matte shading and reflected color
    vec3 matte_shading = shade_matte(ip);
    vec3 color = ray_color(
        ip.ray_level + 1,
        v3_add(ip.p, v3_multiply(ip.n, 0.0001)),
        r
    );

    // Create surface color 75% matte shading and 25% reflected color
    return v3_add(
        v3_multiply(matte_shading, 0.75),
        v3_multiply(color, 0.25)
    );
}

// Returns the shaded color for the given point to shade.
// Calls the relevant shading function based on the material index.
vec3
shade(intersection_point ip)
{
  switch (ip.material)
  {
    case 0:
      return shade_constant(ip);
    case 1:
      return shade_matte(ip);
    case 2:
      return shade_blinn_phong(ip);
    case 3:
      return shade_reflection(ip);
    default:
      return shade_constant(ip);

  }
}

// Determine the surface color for the first object intersected by
// the given ray, or return the scene background color when no
// intersection is found
vec3
ray_color(int level, vec3 ray_origin, vec3 ray_direction)
{
    intersection_point  ip;

    // If this ray has been reflected too many times, simply
    // return the background color.
    if (level >= 3)
        return scene_background_color;

    // Check if the ray intersects anything in the scene
    if (find_first_intersection(&ip, ray_origin, ray_direction))
    {
        // Shade the found intersection point
        ip.ray_level = level;
        return shade(ip);
    }

    // Nothing was hit, return background color
    return scene_background_color;
}
