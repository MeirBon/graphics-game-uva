/* Computer Graphics, Assignment, Volume rendering with cubes/points/isosurface
 *
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 08-12-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 08-12-2017
 * Comments ........
 *
 * (always fill in these fields before submitting!!)
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "marching_tetrahedra.h"

/* Compute a linearly interpolated position where an isosurface cuts
   an edge between two vertices (p1 and p2), each with their own
   scalar value (v1 and v2) */

static vec3
interpolate_points(unsigned char isovalue, vec3 p1, vec3 p2, unsigned char v1,
                   unsigned char v2)
{
    vec3 interpolated;

    /*
     * v1 and v2 are around isovalue, detect the larger and smaller values to
     * find out what to multiply the vertices with.
     */

    if (v1 < v2) {
       interpolated = v3_multiply(v3_add(
           v3_multiply(p1, v2 - isovalue),
           v3_multiply(p2, isovalue - v1)
       ), 1.0 / (v2 - v1));
    } else {
       interpolated = v3_multiply(v3_add(
           v3_multiply(p1, isovalue - v2),
           v3_multiply(p2, v1 - isovalue)
       ), 1.0/ (v1 - v2));
    }

    return interpolated;
}

/*
 * Generate the triangles with values and isovalue for the cell.
 */

int generate_triangle(triangle *triangles, unsigned char isovalue, cell c,
                      int v0, int v1, int v2, int v3, int v4, int v5)
{
    triangles->p[0] = interpolate_points(isovalue, c.p[v0], c.p[v1],
                                         c.value[v0], c.value[v1]);
    triangles->p[1] = interpolate_points(isovalue, c.p[v2], c.p[v3],
                                         c.value[v2], c.value[v3]);
    triangles->p[2] = interpolate_points(isovalue, c.p[v4], c.p[v5],
                                         c.value[v4], c.value[v5]);

    vec3 n = v3_multiply(
        v3_normalize(
            v3_crossprod(
                v3_subtract(triangles->p[0], triangles->p[1]),
                v3_subtract(triangles->p[2], triangles->p[0])
            )
        ),
        -1
    );

    triangles->n[0] = n;
    triangles->n[1] = n;
    triangles->n[2] = n;

    return 1;
}

/* Using the given iso-value generate triangles for the tetrahedron
   defined by corner vertices v0, v1, v2, v3 of cell c.

   Store the resulting triangles in the "triangles" array.

   Return the number of triangles created (either 0, 1, or 2).

   Note: the output array "triangles" should have space for at least
         2 triangles.
*/

static int
generate_tetrahedron_triangles(triangle *triangles, unsigned char isovalue, cell c, int v0, int v1, int v2, int v3)
{
    int bit = 0x00;

    if (c.value[v0] > isovalue) bit += 0x01;
    if (c.value[v1] > isovalue) bit += 0x02;
    if (c.value[v2] > isovalue) bit += 0x04;
    if (c.value[v3] > isovalue) bit += 0x08;

    switch (bit) {
        case 0x00:
        case 0x0F:
            return 0;
        case 0x01:
        case 0x0E:
            return generate_triangle(triangles, isovalue, c, v0, v1, v0, v2, v0, v3);
        case 0x02:
        case 0x0D:
            return generate_triangle(triangles, isovalue, c, v1, v2, v1, v3, v1, v0);
        case 0x04:
        case 0x0B:
            return generate_triangle(triangles, isovalue, c, v2, v1, v2, v3, v2, v0);
        case 0x08:
        case 0x07:
            return generate_triangle(triangles, isovalue, c, v3, v0, v3, v1, v3, v2);
        case 0x03:
        case 0x0C:
            return generate_triangle(triangles, isovalue, c, v1, v3, v1, v2, v3, v0) +
                generate_triangle(triangles, isovalue, c, v3, v0, v0, v2, v2, v1);
        case 0x05:
        case 0x0A:
            return generate_triangle(triangles, isovalue, c, v0, v3, v3, v2, v2, v1) +
                generate_triangle(triangles, isovalue, c, v0, v3, v0, v1, v1, v2);
        case 0x06:
        case 0x09:
            return generate_triangle(triangles, isovalue, c, v3, v2, v2, v0, v3, v1) +
                generate_triangle(triangles, isovalue, c, v0, v1, v0, v2, v3, v1);
        default:
            return 0;
    }
}

/* Generate triangles for a single cell for the given iso-value. This function
   should produce at most 6 * 2 triangles (for which the "triangles" array should
   have enough space).

   Use calls to generate_tetrahedron_triangles().

   Return the number of triangles produced
*/

int
generate_cell_triangles(triangle *triangles, cell c, unsigned char isovalue)
{
    int n_triangles = 0;
    n_triangles += generate_tetrahedron_triangles(&triangles[n_triangles],
                                                  isovalue, c, 0, 1, 3, 7);
    n_triangles += generate_tetrahedron_triangles(&triangles[n_triangles],
                                                  isovalue, c, 0, 1, 5, 7);
    n_triangles += generate_tetrahedron_triangles(&triangles[n_triangles],
                                                  isovalue, c, 0, 2, 3, 7);
    n_triangles += generate_tetrahedron_triangles(&triangles[n_triangles],
                                                  isovalue, c, 0, 2, 6, 7);
    n_triangles += generate_tetrahedron_triangles(&triangles[n_triangles],
                                                  isovalue, c, 0, 4, 5, 7);
    n_triangles += generate_tetrahedron_triangles(&triangles[n_triangles],
                                                  isovalue, c, 0, 4, 6, 7);
    return n_triangles;
}
