/* Computer Graphics and Game Technology, Assignment Box2D game
 *
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 08-12-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 08-12-2017
 * Comments ........
 *
 * (always fill in these fields before submitting!!)
 */

#include <cstdio>
#include <unistd.h>

#ifdef __APPLE__
#include <GLEW/glew.h>
#include <GLUT/glut.h>
#else
#include <GL/glew.h>
#include <GL/glut.h>
#endif

#include <Box2D/Box2D.h>

#include "levels.h"

unsigned int reso_x = 800, reso_y = 600;  // Window size in pixels
const float world_x = 8.f, world_y = 6.f; // Level (world) size in meters

int last_time;
int frame_count;

// Information about the levels loaded from files will be available in these.
unsigned int num_levels;
level_t *levels;

// Global variables
const char* FILENAME = "highscore.txt";
const float PI          = 3.1415926;
const float GRAVITY     = -0.2f;
const float BALL_RADIUS = 0.1;
float TIMESTEP    = 1.0 / 60.0; // 60 fps
int WON = false;
int STARTED = 0;
int HIGHSCORE = 0;
int SCORE = 0;

b2World *world;
b2Body *ball;
unsigned int current_level = 0;

typedef struct polygon {
  bool is_dynamic;
  float x[8], y[8];
  int num_verts;
  float cr;
  float cg;
  float cb;
  joint_t *joints;
  b2Body *body;
  b2Vec2 position;
} polygon;

polygon *polygons;
unsigned int num_polygons;
bool drawing_polygon;

/*
 * Add a new object to the world.
 */
b2Body *create_Box2D_object(polygon *object) {
  b2PolygonShape shape;
  b2BodyDef bodyDef;
  b2Body *body;
  b2FixtureDef fixture;

  b2Vec2 *vertices = new b2Vec2[object->num_verts];
  for (int i = 0; i < object->num_verts; i++) {
    vertices[i].Set(object->x[i], object->y[i]);
  }

  if (object->is_dynamic) {
    bodyDef.type = b2_dynamicBody;
    fixture.friction = 1.0;
    fixture.restitution = 0.0;
    fixture.density = 1.0;
  } else {
    bodyDef.type = b2_staticBody;
    fixture.friction = 1.0;
  }

  shape.Set(vertices, object->num_verts);
  bodyDef.position.x = object->position.x;
  bodyDef.position.y = object->position.y;
  body = world->CreateBody(&bodyDef);
  fixture.shape = &shape;
  body->CreateFixture(&fixture);
  body->ResetMassData();

  delete[] vertices;

  return body;
}

/*
 * Load a given world, i.e. read the world from the `levels' data structure and
 * convert it into a Box2D world.
 */
void load_world(unsigned int level) {
  if (level >= num_levels) {
    // Note that level is unsigned but we still use %d so -1 is shown as
    // such.
    printf("Warning: level %d does not exist.\n", level);
    return;
  }

  drawing_polygon = false;

  current_level = level;

  // Create a Box2D world and populate it with all bodies for this level
  // (including the ball).
  glClear(GL_COLOR_BUFFER_BIT);

  // Initialize world
  b2Vec2 gravity(0.0, GRAVITY);
  world = new b2World(gravity);

  // Create ground
  b2BodyDef groundBodyDef;
  b2Body *groundBody;
  b2PolygonShape groundBox;
  groundBodyDef.position.Set(0.0f, -10.0f);
  groundBody = world->CreateBody(&groundBodyDef);
  groundBox.SetAsBox(50.0f, 10.0f);
  groundBody->CreateFixture(&groundBox, 0.0f);

  b2BodyDef ballBodyDef;             // Define ball
  ballBodyDef.type = b2_dynamicBody; // Define type
  ballBodyDef.position.Set(levels[level].start.x,
                           levels[level].start.y); // starting position
  ball = world->CreateBody(&ballBodyDef); // initialize world ball object

  b2CircleShape dynamicBox;
  dynamicBox.m_radius = BALL_RADIUS;

  b2FixtureDef fixtureDef;
  fixtureDef.shape = &dynamicBox;
  fixtureDef.density = 1.0f;
  fixtureDef.friction = 0.3f;
  ball->CreateFixture(&fixtureDef);

  // generate polygons, add 1 for finish
  polygons = new polygon[levels[level].num_polygons + 1];
  num_polygons = levels[level].num_polygons + 1;

  for (unsigned int p = 0; p < levels[level].num_polygons; p++) {
    poly_t *current_polygon = &levels[level].polygons[p];

    polygons[p].num_verts = current_polygon->num_verts;

    for (unsigned int v = 0; v < current_polygon->num_verts; v++) {
      point_t *vert = &current_polygon->verts[v];
      polygons[p].x[v] = vert->x;
      polygons[p].y[v] = vert->y;
      polygons[p].cr = 0.0;
      polygons[p].cg = 1.0;
      polygons[p].cb = 0.0;
      polygons[p].is_dynamic = current_polygon->is_dynamic;
      polygons[p].position.Set(current_polygon->position.x, current_polygon->position.y);
    }
  }

  // Create ending
  int index = num_polygons - 1;
  polygons[index].num_verts = 8;
  polygons[index].x[0] =  -0.05;
  polygons[index].x[1] =  -0.05;
  polygons[index].x[2] =  -0.05;
  polygons[index].x[3] = 0.0;
  polygons[index].x[4] = 0.05;
  polygons[index].x[5] = 0.05;
  polygons[index].x[6] = 0.05;
  polygons[index].x[7] = 0.0;

  polygons[index].y[0] = -0.05;
  polygons[index].y[1] = 0.0;
  polygons[index].y[2] = 0.05;
  polygons[index].y[3] = 0.05;
  polygons[index].y[4] = 0.05;
  polygons[index].y[5] = 0.0;
  polygons[index].y[6] = -0.05;
  polygons[index].y[7] = -0.05;

  polygons[index].cr = 0.0;
  polygons[index].cg = 0.0;
  polygons[index].cb = 1.0;
  polygons[index].is_dynamic = false;
  polygons[index].position.Set(levels[level].end.x, levels[level].end.y);

  // Add all polygons to world
  for (unsigned int i = 0; i < num_polygons; i++) {
    polygons[i].body = create_Box2D_object(&polygons[i]);
  }

  for (unsigned int i = 0; i < levels[level].num_joints; i++) {
    joint_t *cur_joint = &levels[level].joints[i];

    if (cur_joint->joint_type == JOINT_PULLEY) {
      b2PulleyJointDef jointDef;

      b2Vec2 groundAnchorA;
      b2Vec2 groundAnchorB;
      b2Vec2 localAnchorA;
      b2Vec2 localAnchorB;
      groundAnchorA.Set(cur_joint->pulley.ground1.x, cur_joint->pulley.ground1.y);
      groundAnchorB.Set(cur_joint->pulley.ground2.x, cur_joint->pulley.ground2.y);
      localAnchorA.Set(cur_joint->anchor.x, cur_joint->anchor.y);
      localAnchorB.Set(cur_joint->pulley.anchor2.x, cur_joint->pulley.anchor2.y);

      jointDef.Initialize(
        polygons[cur_joint->objectA].body,
        polygons[cur_joint->objectB].body,
        groundAnchorA,
        groundAnchorB,
        localAnchorA,
        localAnchorB,
        cur_joint->pulley.ratio
      );
      
      world->CreateJoint(&jointDef);
    } else if (cur_joint->joint_type == JOINT_REVOLUTE) {
      b2RevoluteJointDef jointDef;
      b2Vec2 anchor;
      anchor.Set(cur_joint->anchor.x, cur_joint->anchor.y);

      jointDef.Initialize(
        polygons[cur_joint->objectA].body,
        polygons[cur_joint->objectB].body,
        anchor
      );
      world->CreateJoint(&jointDef);
    }
  }
}

/**
 * Save highscore to file
 */
void save_high_score() {
  FILE *input = fopen(FILENAME, "w");
  if (input != NULL)
  {
      char buffer[10];
      sprintf(buffer, "%d", HIGHSCORE);
      fputs(buffer, input);
      fclose(input);
  }
}

/**
 * Get highscore from saved file
 */
void load_high_score() {
  if( access(FILENAME, F_OK ) != -1 ) {
    FILE *input = fopen(FILENAME, "r");
    char line[10];
    fgets(line, 100, input);
    HIGHSCORE = atoi(line);
    fclose(input);
  }
}

/**
 * Check if new highscore is lower than saved value
 */
void update_highscore() {
  SCORE = (STARTED - time(0)) * - 1;
  if (SCORE < HIGHSCORE || HIGHSCORE == 0) {
    HIGHSCORE = SCORE;
    save_high_score();
  }
}

/**
 * Resets the game to lvl 0
 */
void reset() {
  load_high_score();
  WON = false;
  current_level = 0;
  STARTED = time(0);
  load_world(current_level);
}

/*
 * Draws a circle.
 */
void draw_circle(float x, float y, float r,
                 float cr, float cg, float cb) {
  float rad;
  glColor3f(cr, cg, cb);
  GLuint buffer[1];
  GLfloat *vertices = new GLfloat[720];

  // loop over 360 degrees
  for (int i = 0; i < 360; i++) {
    rad = i * PI / 180.0;
    // calculate points
    vertices[i * 2] = cos(rad) * r + x;
    vertices[i * 2 + 1] = sin(rad) * r + y;
  }

  glGenBuffers(1, buffer);
  glBindBuffer(GL_ARRAY_BUFFER, *buffer);
  glBufferData(
    GL_ARRAY_BUFFER, sizeof(GLfloat) * 720,
    vertices, GL_STATIC_DRAW
  );
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(2, GL_FLOAT, 0, 0);
  glDrawArrays(GL_TRIANGLE_FAN, 0, 360);
  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(1, buffer);
  delete[] vertices;
}

/**
 * Draws a polygon
 */
void draw_polygon(float *x, float *y, int num_verts, float cr, float cg, float cb, float diff_x, float diff_y, float angle) {
  glColor3f(cr, cg, cb);
  GLuint buffer[1];
  GLfloat *vertices = new GLfloat[num_verts * 2];

  for (int i = 0; i < num_verts; i++) {
    vertices[i * 2] = x[i];
    vertices[i * 2 + 1] = y[i];
  }

  glGenBuffers(1, buffer);
  glBindBuffer(GL_ARRAY_BUFFER, *buffer);
  glBufferData(
    GL_ARRAY_BUFFER, sizeof(GLfloat) * num_verts * 2,
    vertices, GL_STATIC_DRAW
  );

  glTranslatef(diff_x, diff_y, 0.0); // Translate to current position
  glRotatef(angle * 180 / PI, 0, 0, 1); // Rotate to current angle

  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(2, GL_FLOAT, 0, 0);
  glDrawArrays(GL_TRIANGLE_FAN, 0, num_verts);
  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(1, buffer);
  delete[] vertices;
}

/*
 * Calculate the distance between two points.
 */
float distance(float x1, float y1, float x2, float y2) {
  return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

/*
 * Check if the player has won.
 */
bool check_won(float x1, float y1, float x2, float y2) {
  return distance(x1, y1, x2, y2) < 0.25f;
}

/*
 * Called when we should redraw the scene (i.e. every frame).
 * It will show the current framerate in the window title.
 */
void draw(void) {
  int time = glutGet(GLUT_ELAPSED_TIME);
  int frametime = time - last_time;
  frame_count++;

  // Clear the buffer
  glColor3f(0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  //
  // Do any logic and drawing here.
  //
  world->Step(TIMESTEP, 1, 1);
  b2Vec2 position = ball->GetPosition();

  // If player has won, load next level
  if (check_won(
        position.x, position.y,
        levels[current_level].end.x, levels[current_level].end.y
      )) {
    current_level += 1;
    if (current_level > 4) {
      WON = true;
      update_highscore();
    } else {
      load_world(current_level);
    }
  }

  // Draw ball of player
  glLoadIdentity(); // Init identity matrix everytime we start drawing
  draw_circle(position.x, position.y, BALL_RADIUS, 1.0, 0.0, 0.0);

  // Draw polygons of world
  for (unsigned int i = 0; i < num_polygons; i++) {
    b2Vec2 poly_position = polygons[i].body->GetPosition();
    glLoadIdentity();
    draw_polygon(
      polygons[i].x, polygons[i].y, polygons[i].num_verts,
      polygons[i].cr, polygons[i].cg, polygons[i].cb,
      poly_position.x, poly_position.y, polygons[i].body->GetAngle()
    );
  }

  // Show rendered frame
  glutSwapBuffers();

  // Display fps in window title.
  if (frametime >= 1000) {
    char window_title[128];
    float fps = frame_count / (frametime / 1000.f);
    // If user has won: show instructions to restart
    if (WON) {
      snprintf(window_title, 128, "Box2D: %f fps, Won! Score: %d, HighScore: %d seconds, press r to restart.",
             fps, SCORE, HIGHSCORE);
    } else if (HIGHSCORE != 0) {
      snprintf(window_title, 128, "Box2D: %f fps, level %d/%d, HighScore: %d seconds",
              fps, current_level + 1, num_levels, HIGHSCORE);
    } else {
      snprintf(window_title, 128, "Box2D: %f fps, level %d/%d, HighScore: -",
              fps, current_level + 1, num_levels);
    }

    /**
     * Update TIMESTEP according to FPS to make physics run correctly
     * synchronized with framerate
     */
    TIMESTEP = 1.0 / fps;

    glutSetWindowTitle(window_title);
    last_time = time;
    frame_count = 0;
  }
}

/*
 * Called when window is resized. We inform OpenGL about this, and save this
 * for future reference.
 */
void resize_window(int width, int height) {
  glViewport(0, 0, width, height);
  reso_x = width;
  reso_y = height;
}

/*
 * Called when the user presses a key.
 */
void key_pressed(unsigned char key, int x, int y) {
  switch (key) {
    case 27: // Esc
    case 'q':
      exit(0);
      break;
    case 'r':
      reset();
    // Add any keys you want to use, either for debugging or gameplay.
    default:
      break;
  }
}

/*
 * Called when the user clicked (or released) a mouse buttons inside the window.
 */
void mouse_clicked(int button, int state, int x, int y) {
  if (state != 1) {
    return;
  }

  /*
   * Create a new polygon.
   */
  if (!drawing_polygon) {
    polygon *temp_polygons = polygons;
    polygons = new polygon[num_polygons + 1];
    polygons[num_polygons].num_verts = 0;
    polygons[num_polygons].cr = 0.0;
    polygons[num_polygons].cg = 1.0;
    polygons[num_polygons].cb = 0.0;
    polygons[num_polygons].is_dynamic = true;
    for (unsigned int p = 0; p < num_polygons; p++) {
      polygons[p] = temp_polygons[p];
    }
    delete[] temp_polygons;
    drawing_polygon = true;
  }

  /*
   * Add vertex to polygon or finish polygon.
   */
  unsigned int num_verts = polygons[num_polygons].num_verts;
  if (num_verts < 4) {
    polygons[num_polygons].x[num_verts] = (float)x / reso_x * world_x;
    polygons[num_polygons].y[num_verts] = -1.0f * ((float)y - reso_y) / reso_y *
        world_y;
    polygons[num_polygons].num_verts++;
    if (num_verts == 3) {
      drawing_polygon = false;

      /*
       * Check if the vertices are too close together.
       */
      for (int i = 0; i < 4; i++) {
        int other = (i + 1) % 4;
        if (distance(
            polygons[num_polygons].x[i],
            polygons[num_polygons].y[i],
            polygons[num_polygons].x[other],
            polygons[num_polygons].y[other]
          ) < 0.05f) {
          printf("Polygon is too small.\n");
          return;
        }
      }

      polygons[num_polygons].position.SetZero();
      polygons[num_polygons].body = create_Box2D_object(&polygons[num_polygons]);
      num_polygons++;
    }
  }
}

/*
 * Called when the mouse is moved to a certain given position.
 */
void mouse_moved(int x, int y) {}

int main(int argc, char **argv) {
  // Create an OpenGL context and a GLUT window.
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowSize(reso_x, reso_y);
  glutCreateWindow("Box2D");

  glewInit();

  // Bind all GLUT events do callback function.
  glutDisplayFunc(&draw);
  glutIdleFunc(&draw);
  glutReshapeFunc(&resize_window);
  glutKeyboardFunc(&key_pressed);
  glutMouseFunc(&mouse_clicked);
  glutMotionFunc(&mouse_moved);
  glutPassiveMotionFunc(&mouse_moved);

  // Initialise the matrices so we have an orthogonal world with the same size
  // as the levels, and no other transformations.
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, world_x, 0, world_y, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Read the levels into a bunch of structs.
  num_levels = load_levels(&levels);
  printf("Loaded %d levels.\n", num_levels);

  // Load the first level (i.e. create all Box2D stuff).
  reset(); // Replace with call to reset to init highscore

  last_time = glutGet(GLUT_ELAPSED_TIME);
  frame_count = 0;
  glutMainLoop();

  return 0;
}
