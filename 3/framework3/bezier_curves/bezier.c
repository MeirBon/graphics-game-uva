/* Computer Graphics, Assignment, Bezier curves
 * Filename ........ bezier.c
 * Description ..... Bezier curves
 * Date ............ 22.07.2009
 * Created by ...... Paul Melis
 *
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 02-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 31-10-2017
 * Comments ........
 */

#include <math.h>
#include "bezier.h"
#include <stdio.h>

/*
 * Calculate the factorial of `i`.
 */
int factorial(int i)
{
    if (i == 0) {
        return 1;
    }

    return i * factorial(i - 1);
}

/*
 * Calculate the binomial distribution of `n` and `k` with `n` choose `k`.
 */
float binomial_coefficient(int n, int k)
{
    return factorial(n) / (float)(factorial(k) * factorial(n - k));
}

/*
 * Calculate the berstein polynomial of degree `n`, with number `i` and curve
 * parameter `u`.
 */
float bernstein_polynomial(int n, int i, float u)
{
    return binomial_coefficient(n, i) * pow(u, i) * pow(1 - u, n - i);
}

/*
 * Given a Bezier curve defined by the 'num_points' control points
 * in 'p' compute the position of the point on the curve for parameter
 * value 'u'.
 *
 * Return the x and y values of the point by setting *x and *y,
 * respectively.
 */
void
evaluate_bezier_curve(float *x, float *y, control_point p[], int num_points, float u)
{
    float bernstein;
    int n = num_points - 1;
    *x = 0.0;
    *y = 0.0;

    for (int i = 0; i < num_points; i++) {
        bernstein = bernstein_polynomial(n, i, u);
        *x += bernstein * p[i].x;
        *y += bernstein * p[i].y;
    }
}

/*
 * Draw a Bezier curve defined by the control points in p[], which
 * will contain 'num_points' points.
 *
 *
 * The 'num_segments' parameter determines the "discretization" of the Bezier
 * curve and is the number of straight line segments that should be used
 * to approximate the curve.
 *
 * Call evaluate_bezier_curve() to compute the necessary points on
 * the curve.
 *
 * You will have to create a GLfloat array of the right size, filled with the
 * vertices in the appropriate format and bind this to the buffer.
 */
void
draw_bezier_curve(int num_segments, control_point p[], int num_points)
{
    GLuint buffer[1];

    /* Write your own code to create and fill the array here. */
    GLfloat vertices[(num_segments + 1) * 2];
    float curve_parameter_size = 1.0 / num_segments;

    for (int i = 0; i <= num_segments; i++) {
        float x, y;
        evaluate_bezier_curve(&x, &y, p, num_points, i * curve_parameter_size);
        vertices[i * 2] = (GLfloat)x;
        vertices[i * 2 + 1] = (GLfloat)y;
    }

    // This creates the VBO and binds an array to it.
    glGenBuffers(1, buffer);
    glBindBuffer(GL_ARRAY_BUFFER, *buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * (num_segments + 1) * 2,
                 &vertices, GL_STATIC_DRAW);

    // This tells OpenGL to draw what is in the buffer as a Line Strip.
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, 0);
    glDrawArrays(GL_LINE_STRIP, 0, (num_segments + 1));
    glDisableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, buffer);
}

/*
 * Find the intersection of a cubic Bezier curve with the line X=x.
 * Return 1 if an intersection was found and place the corresponding y
 * value in *y.
 * Return 0 if no intersection exists.
 */
int
intersect_cubic_bezier_curve(float *y, control_point p[], float x)
{
    float tmp_x, tmp_y, u;

    // Check if x is inside curve
    if (x < p[0].x || x > p[3].x) {
        return 0;
    }


    // Approximation of 10**-3
    for (u = 0.0; u < 1.0; u += 0.001) {
        evaluate_bezier_curve(&tmp_x, &tmp_y, p, 4, u);
        if (fabs(tmp_x - x) < 0.001) {
            *y = tmp_y;
            return 1;
        }
    }
    return 0;
}
