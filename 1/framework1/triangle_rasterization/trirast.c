/* Computer Graphics assignment, Triangle Rasterization
 * Filename ........ trirast.c
 * Description ..... Implements triangle rasterization
 * Created by ...... Paul Melis
 *
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 02-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 31-10-2017
 * Comments ........
 *
 *
 * (always fill in these fields before submitting!!)
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "types.h"

/*
 * Rasterize a single triangle.
 * The triangle is specified by its corner coordinates
 * (x0,y0), (x1,y1) and (x2,y2).
 * The triangle is drawn in color (r,g,b).
 */

/*
 * Returns the largest number of the three arguments `a`, `b` and `c`.
 */
float largest_number(float a, float b, float c)
{
    float large = a;

    if (b > large) {
        large = b;
    }
    if (c > large) {
        large = c;
    }

    return large;
}


/*
 * Return the smallest number of the three arguments `a`, `b` and `c`.
 */
float smallest_number(float a, float b, float c)
{
    float small = a;

    if (b < small) {
        small = b;
    }
    if (c < small) {
        small = c;
    }

    return small;
}


/*
 * Calculation of the formule used to determine if a point is in the triangle
 * in the algorithm implemented in draw_triangle() and
 * draw_triangle_optimized().
 */
float line(float x, float y, float x0, float y0, float x1, float y1)
{
    return (y0 - y1) * x + (x1 - x0) * y + x0 * y1 - x1 * y0;
}
   

/*
 * Draws the pixels for a triangle with corners at (`x0`, `y0`), (`x1`, `y1`)
 * and (`x2`, `y2`) and with color `r`, `g` and `b`. The implemened algorithm
 * is the algorithm from the book 'Fundamentals of Computer Graphics' by Peter
 * Shirley on page 66.
 */
void
draw_triangle(float x0, float y0, float x1, float y1, float x2, float y2,
    byte r, byte g, byte b)
{
    float alpha, beta, gamma;
    float x_min = floor(smallest_number(x0, x1, x2));
    float x_max = ceil(largest_number(x0, x1, x2));
    float y_min = floor(smallest_number(y0, y1, y2));
    float y_max = ceil(largest_number(y0, y1, y2));
    float line_a = line(x0, y0, x1, y1, x2, y2);
    float line_b = line(x1, y1, x2, y2, x0, y0);
    float line_c = line(x2, y2, x0, y0, x1, y1);

    for (float y = y_min; y <= y_max; y++) {
        for (float x = x_min; x <= x_max; x++) {
            alpha = line(x, y, x1, y1, x2, y2) / line_a;
            beta = line(x, y, x2, y2, x0, y0) / line_b;
            gamma = line(x, y, x0, y0, x1, y1) / line_c;
            if (alpha >= 0 && beta >= 0 && gamma >= 0) {
                if ((alpha > 0 || line_a * line(-1, -1, x1, y1, x2, y2) > 0) &&
                    (beta > 0 || line_b * line(-1, -1, x2, y2, x0, y0) > 0) &&
                    (gamma > 0 || line_c * line(-1, -1, x0, y0, x1, y1) > 0)) {
                    PutPixel(x, y, r, g, b);
                }
            }
        }
    }
}


/*
 * Draws the pixels for a triangle with corners at (`x0`, `y0`), (`x1`, `y1`)
 * and (`x2`, `y2`) and with color `r`, `g` and `b`. The implemened algorithm
 * is the algorithm from the book 'Fundamentals of Computer Graphics' by Peter
 * Shirley on page 66. The implementation here has been optimized.
 */
void
draw_triangle_optimized(float x0, float y0, float x1, float y1, float x2,
    float y2, byte r, byte g, byte b)
{
    float alpha, beta, gamma;
    float x_min = floor(smallest_number(x0, x1, x2));
    float x_max = ceil(largest_number(x0, x1, x2));
    float y_min = floor(smallest_number(y0, y1, y2));
    float y_max = ceil(largest_number(y0, y1, y2));
    float line_a = line(x0, y0, x1, y1, x2, y2);
    float line_b = line(x1, y1, x2, y2, x0, y0);
    float line_c = line(x2, y2, x0, y0, x1, y1);

    /*
     * Calculate beforehand, since they will not change during calculation of
     * all pixels in the square.
     */
    char line_outside_a = line_a * line(-1, -1, x1, y1, x2, y2) > 0;
    char line_outside_b = line_b * line(-1, -1, x2, y2, x0, y0) > 0;
    char line_outside_c = line_c * line(-1, -1, x0, y0, x1, y1) > 0;

    for (float y = y_min; y <= y_max; y++) {
        for (float x = x_min; x <= x_max; x++) {
            alpha = line(x, y, x1, y1, x2, y2) / line_a;
            beta = line(x, y, x2, y2, x0, y0) / line_b;
            gamma = line(x, y, x0, y0, x1, y1) / line_c;
            if (alpha >= 0 && beta >= 0 && gamma >= 0) {
                if ((alpha > 0 || line_outside_a) &&
                    (beta > 0 || line_outside_b) &&
                    (gamma > 0 || line_outside_c)) {
                    PutPixel(x, y, r, g, b);
                }
            }
        }
    }
}

