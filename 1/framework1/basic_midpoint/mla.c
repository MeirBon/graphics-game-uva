/* Computer Graphics, Assignment 1, Bresenham's Midpoint Line-Algorithm
 *
 * Filename ........ mla.c
 * Description ..... Midpoint Line Algorithm
 * Created by ...... Jurgen Sturm
 *
 * Student name .... Auke Schuringa
 * Student email ... auke.schuringa@student.uva.nl
 * Collegekaart .... 11023465
 * Date ............ 02-11-2017
 * Comments ........
 *
 * Student name .... Mèir Noordermeer
 * Student email ... meirnoordermeer@me.com
 * Collegekaart .... 11341335
 * Date ............ 31-10-2017
 * Comments ........
 *
 *
 * (always fill in these fields before submitting!!)
 */

#include "SDL.h"
#include "init.h"

/**
 * f(x,y) = (y0 - y1) * x + (x1 - x0) * y + x0 * y1 - x1 * y0
 */
int CalcD(int y0, int y1, int x0, int x1, int x, int y) {
  return (y0 - y1) * x + (x1 - x0) * y + 2*(x0 * y1) - 2*(x1 * y0);
}

// swaps pointers of 2 ints
void swap(int* a, int* b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}

/*
 * Midpoint Line Algorithm
 *
 * As you probably will have figured out, this is the part where you prove
 * your programming skills. The code in the mla function should draw a direct
 * line between (x0,y0) and (x1,y1) in the specified color.
 *
 * Until now, the example code below draws only a horizontal line between
 * (x0,y0) and (x1,y0) and a vertical line between (x1,y1).
 *
 * And here the challenge begins..
 *
 * Good luck!
 *
 *
 */
void mla(SDL_Texture *t, int x0, int y0, int x1, int y1, Uint32 colour) {
  int x, y;
  int dx = x1 - x0, dy = y1 - y0;
  int dy_negative = 0, swap_xy = 0;
  int d, dd, d_else;

  // draw endpoint pixel
  PutPixel(t, x1, y1, colour);

  // swap x0 and x1 if on left side of circle
  if (x1 < x0) {
    swap(&x0, &x1);
    swap(&y0, &y1);
  }

  /** 
   * check if slope of y is negative, if so switch y values
   *
   * important! -> recalculate dy in if statement
   * otherwise dy is rounded up to 0 and if statement won't be accurate
   */
  if ((y1 - y0) < 0) {
    dy_negative = 1;
    swap(&y0, &y1);
  }

  // check if dx is smaller than dy, if so use y as the x-axis in drawing
  if (abs(dx) < abs(dy)) {
    swap(&x0, &y0);
    swap(&x1, &y1);
    swap_xy = 1;
  }

  /**
   * use a factor of 2 to be able to use only integers
   * Fundamentals of Computer Graphics, page 60
   */
  d = CalcD(y0, y1, x0, x1, 2*(x0 + 1), (2*y0 + 1));
  dd = 2 * (x1 - x0) + 2 * (y0 - y1);
  d_else = 2 * (y0 - y1);

  for (x = x0, y = y0; x <= x1; x++) {
    int final_y = y;

    if (dy_negative == 1) {
      final_y = y0 - (y - y1);
    }

    if (swap_xy == 1) {
      PutPixel(t, final_y, x, colour);
    } else {
      PutPixel(t, x, final_y, colour);
    }

    if (d < 0) {
      y++;
      d += dd;
    } else {
      d += d_else;
    }
  }

  return;
}
